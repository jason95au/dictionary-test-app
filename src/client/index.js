import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import ReactGA from 'react-ga';
import createHistory from 'history/createBrowserHistory';

import store from './redux/store';
import SearchPage from './pages/SearchPage';
import WoTelSatPage from './pages/WoTelSatPage';
import NoMatch from './pages/NoMatch';

const history = createHistory();

const GAToken = 'UA-108436770-4';
ReactGA.initialize(GAToken);

// ga setup
history.listen(location => {
  ReactGA.set({ page: location.pathname });
  ReactGA.pageview(location.pathname);
});

render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route exact path="/dictionary/:phrase" component={WoTelSatPage} />
        <Route component={NoMatch} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
