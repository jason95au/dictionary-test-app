// import Raven from 'raven-js';
import { createStore, applyMiddleware } from 'redux';
// import createRavenMiddleware from 'raven-for-redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index';

// Sentry config
// Raven.config("<YOUR_DSN>").install();

const store = createStore(rootReducer, applyMiddleware(thunk));
// Sentry config
// const store = createStore(rootReducer, applyMiddleware(createRavenMiddleware(Raven, {})));
export default store;
