import store from './store';

describe('redux store', () => {
  test('is initialized properly', () => {
    expect(store.getState()).toMatchSnapshot();
  });
});
