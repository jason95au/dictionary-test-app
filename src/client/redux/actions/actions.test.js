import configureStore from 'redux-mock-store';
import { addArticle, initializeArticles } from './index';

const mockStore = configureStore();
const store = mockStore();

describe('redux actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  test('addArticle dispatches the correct action and payload', () => {
    store.dispatch(addArticle({ title: 'article 1', id: '1234' }));
    expect(store.getActions()).toMatchSnapshot();
  });

  test('initializeArticles dispatches the correct action and payload', () => {
    store.dispatch(
      initializeArticles([{ title: 'article 1', id: '1234' }, { title: 'article s', id: '12345' }])
    );
    expect(store.getActions()).toMatchSnapshot();
  });
});
