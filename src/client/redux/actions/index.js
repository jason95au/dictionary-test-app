import axios from 'axios';
import {
  GET_SEARCH_WORD_REQUEST,
  GET_SEARCH_WORD_SUCCESS,
  GET_SEARCH_WORD_FAILURE,
  GET_WORD_DETAILS_REQUEST,
  GET_WORD_DETAILS_SUCCESS,
  GET_WORD_DETAILS_FAILURE,
  GET_LCAT_REQUEST,
  GET_LCAT_SUCCESS,
  GET_LCAT_FAILURE
} from '../constants/action-types';

export const BASE_URL = 'http://localhost:8080';

export function possibleWords(queryWord) {
  if (queryWord.length === 0) {
    return {
      type: GET_SEARCH_WORD_SUCCESS,
      // this paylaod mimics what is expected from Oxford endpoint
      payload: {
        data: {
          metadata: null,
          results: []
        }
      }
    };
  }
  return function asyncAction(dispatch) {
    dispatch({ type: GET_SEARCH_WORD_REQUEST });
    axios({
      method: 'get',
      url: '/search/en',
      baseURL: BASE_URL,
      params: {
        q: queryWord
      }
    })
      .then(response => {
        dispatch({
          type: GET_SEARCH_WORD_SUCCESS,
          payload: response
        });
      })
      .catch(error => {
        dispatch({
          type: GET_SEARCH_WORD_FAILURE,
          payload: error
        });
      });
  };
}

export function getWordInfo(wordID) {
  return function asyncAction(dispatch) {
    dispatch({ type: GET_WORD_DETAILS_REQUEST });
    axios({
      method: 'get',
      url: `/entries/en/${wordID}`,
      baseURL: BASE_URL,
      responseType: 'json'
    })
      .then(response => {
        dispatch({
          type: GET_WORD_DETAILS_SUCCESS,
          payload: response
        });
      })
      .catch(error => {
        dispatch({
          type: GET_WORD_DETAILS_FAILURE,
          payload: error
        });
      });
  };
}

export function getLCATEvaluation(content, mp3Base64) {
  return function asyncAction(dispatch) {
    dispatch({ type: GET_LCAT_REQUEST });
    axios({
      method: 'post',
      url: `/test`,
      baseURL: BASE_URL,
      data: {
        content,
        mp3Base64
      }
    })
      .then(response => {
        dispatch({
          type: GET_LCAT_SUCCESS,
          payload: response
        });
      })
      .catch(error => {
        dispatch({
          type: GET_LCAT_FAILURE,
          payload: error
        });
      });
  };
}
