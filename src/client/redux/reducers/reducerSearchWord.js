import { GET_SEARCH_WORD_SUCCESS, GET_SEARCH_WORD_FAILURE } from '../constants/action-types';

const INITIAL_STATE = {
  metadata: null,
  results: []
};

const searchType = ['headword', 'inflection'];

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_SEARCH_WORD_SUCCESS: {
      const { payload } = action;
      const headWordOnly = payload.data.results.filter(({ matchType }) =>
        searchType.includes(matchType)
      );

      // Bug relating to semantic-ui-react search component where undefined props break the app
      const fittedForSemanticSearch = headWordOnly.map(({ word, id }) => ({
        title: word,
        description: id
      }));
      return {
        metadata: payload.data.metadata,
        results: fittedForSemanticSearch
      };
    }
    case GET_SEARCH_WORD_FAILURE: {
      return INITIAL_STATE;
    }
    default: {
      return state;
    }
  }
}
