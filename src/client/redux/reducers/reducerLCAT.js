import { GET_LCAT_SUCCESS, GET_LCAT_FAILURE } from '../constants/action-types';
import formatScores from '../../helpers/formatScores';

const INITIAL_STATE = {};

const thresholds = {
  phonemeThresOrange: 0.05,
  phonemeThresGreen: 0.1,
  wordThresOrange: 0.5,
  wordThresGreen: 1,
  utteranceThresOrange: 0.5,
  utteranceThresGreen: 1,
  phonemeThres1: 0,
  phonemeThres2: 0.125,
  phonemeThres3: 0.25,
  phonemeThres4: 0.375,
  phonemeThres5: 0.5,
  phonemeThres6: 0.625,
  phonemeThres7: 0.75,
  phonemeThres8: 0.875
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_LCAT_SUCCESS: {
      const { data, content } = action.payload.data;
      const [scores] = formatScores(data[0], content, thresholds);
      return scores;
    }
    case GET_LCAT_FAILURE: {
      return { error: true, message: 'Phoneme score prediction failed' };
    }
    default: {
      return state;
    }
  }
}
