import { GET_WORD_DETAILS_SUCCESS, GET_WORD_DETAILS_FAILURE } from '../constants/action-types';

const INITIAL_STATE = {
  wordID: null,
  word: null,
  lexicalEntries: null
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_WORD_DETAILS_SUCCESS: {
      const { results } = action.payload.data;
      const result = results[0];
      const doc = {
        wordID: result.id,
        word: result.word,
        lexicalEntries: result.lexicalEntries
      };
      return doc;
    }
    case GET_WORD_DETAILS_FAILURE: {
      return INITIAL_STATE;
    }
    default: {
      return state;
    }
  }
}
