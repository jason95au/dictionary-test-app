import { combineReducers } from 'redux';
import reducerWordInfo from './reducerWordInfo';
import reducerSearchWord from './reducerSearchWord';
import reducerLoader from './reducerLoader';
import reducerLCAT from './reducerLCAT';

const rootReducer = combineReducers({
  reducerWordInfo,
  reducerSearchWord,
  reducerLoader,
  reducerLCAT
});

export default rootReducer;
