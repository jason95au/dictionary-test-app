import rootReducer from './index';
import { ADD_ARTICLE, INITIALIZE_ARTICLES } from '../constants/action-types';

describe('rootReducer', () => {
  test('initial state is correct', () => {
    const action = { type: 'dummy_action' };
    const initialState = { articles: [] };

    expect(rootReducer(undefined, action)).toEqual(initialState);
  });

  test('ADD_ARTICLE returns the correct state', () => {
    const article = { title: 'test article', id: '1234' };
    const action = { type: ADD_ARTICLE, payload: article };
    const expectedState = { articles: [article] };

    expect(rootReducer(undefined, action)).toEqual(expectedState);
  });

  test('INITIALIZE_ARTICLES returns the correct state', () => {
    const article1 = { title: 'test article 1', id: '12345' };
    const article2 = { title: 'test article 2', id: '12346' };
    const action = { type: INITIALIZE_ARTICLES, payload: [article1, article2] };
    const expectedState = { articles: [article1, article2] };

    expect(rootReducer(undefined, action)).toEqual(expectedState);
  });
});
