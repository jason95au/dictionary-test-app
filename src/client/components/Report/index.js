import React, { Component } from 'react';
import {
  Grid,
  Segment,
  Accordion,
  Icon,
  Label,
  Popup,
  Button,
  Message,
  Divider,
  Loader,
  Confirm,
  Modal
} from 'semantic-ui-react';
import { Howl } from 'howler';
import LaddaButton, { XL, EXPAND_RIGHT } from 'react-ladda';

import './Report.scss';

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scoreType: 0,
      duration: false,
      playing: false,
      progress: 0,
      activeIndex: 0
    };

    this.renderWords = this.renderWords.bind(this);
    this.renderPhonemes = this.renderPhonemes.bind(this);
    this.handleClickAccordion = this.handleClickAccordion.bind(this);

    this.playWord = this.playWord.bind(this);
    this.playPhoneme = this.playPhoneme.bind(this);
    this.playUtterance = this.playUtterance.bind(this);
    this.progressStep = this.progressStep.bind(this);
  }

  componentDidMount() {
    const { scores } = this.props;
    if (scores) {
      const sprites = {};
      scores.forEach((score, index) => {
        // add word to sprites
        const offset = score.time_start * 1000;
        const duration = score.time_end * 1000 - offset;
        sprites[`word-${index}`] = [offset, duration];
        // add phonemes to sprites
        score.phonemes.forEach((phonemeScore, phonemeIndex) => {
          const phonemeOffset = phonemeScore.time_start * 1000;
          const phonemeDuration = phonemeScore.time_end * 1000 - offset;
          sprites[`word-${index}-phoneme-${phonemeIndex}`] = [phonemeOffset, phonemeDuration];
        });
      });

      this.audio = new Howl({
        src: [this.props.audioURL],
        volume: 0.5,
        format: ['mp3'],
        sprite: sprites,
        onload: () => {
          this.audio._sprite.utterance = [0, this.audio.duration() * 1000];
          this.setState({ duration: this.audio.duration() });
        },
        onplay: () => {
          requestAnimationFrame(this.progressStep);
        },
        onend: () => {
          this.setState({
            playing: false,
            progress: 0
          });
        }
      });
    }
  }

  handleClickAccordion(e, titleProps) {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;
    this.setState({ activeIndex: newIndex });
  }

  playWord(sprite) {
    this.audio.play(sprite);
  }

  playPhoneme(sprite) {
    this.audio.play(sprite);
  }

  playUtterance() {
    if (!this.state.playing) {
      this.setState({
        playing: true,
      });
      this.audio.play('utterance');
    }
  }

  progressStep() {
    const position = this.audio.seek() || 0;
    const progress = position / this.audio.duration();
    this.setState({
      progress
    });
    if (this.state.playing) {
      requestAnimationFrame(this.progressStep);
    }
  }


  renderWords() {
    return this.props.scores.map((wordScore, index) => (
      <Popup
        key={`${wordScore.label}_${wordScore.score}`}
        trigger={(
          <div className="phoneme-score-group">
            <Label
              as="a"
              color={wordScore.colorScore}
              key={`${wordScore.label}_${wordScore.score}`}
              size="big"
              className="report-label"
            >
              {wordScore.label}
            </Label>
            {this.state.scoreType === 0 ? (
              <span className="phoneme-score-label">{Math.floor(wordScore.score * 100)}%</span>
            ) : (
                <span className="phoneme-score-label">{wordScore.numberScore}/8</span>
              )}
          </div>
        )}
        content={(
          <div>
            {/* <h1 className="report-phoneme-popup">{Math.floor(wordScore.score * 100)}%</h1> */}
            <Button
              icon
              color="teal"
              size="big"
              onClick={() => {
                this.playWord(`word-${index}`);
              }}
            >
              <Icon name="volume up" />
            </Button>
          </div>
        )}
        on="click"
        position="top center"
      />
    ));
  }

  renderPhonemes() {
    const isAdmin = true;

    return this.props.scores.map((wordScore, index) => {
      let phonemeScores;
      if (wordScore.phonemes) {
        phonemeScores = wordScore.phonemes.map((phonemeScore, phonemeIndex) => (
          <Popup
            key={`${phonemeScore.label}_${phonemeScore.score}`}
            trigger={
              <div className="phoneme-score-group">
                <Label as="a" color={phonemeScore.colorScore} size="big" className="report-label">
                  {phonemeScore.label}
                </Label>
                {this.state.scoreType === 0 ? (
                  <span className="phoneme-score-label">
                    {Math.floor(phonemeScore.score * 100)}%
                  </span>
                ) : (
                    <span className="phoneme-score-label">{phonemeScore.numberScore}/8</span>
                  )}
              </div>
            }
            content={(
              <div>
                {isAdmin ? (
                  <Button
                    icon
                    color="teal"
                    size="big"
                    onClick={() => {
                      this.playPhoneme(`word-${index}-phoneme-${phonemeIndex}`);
                    }}
                  >
                    <Icon name="volume up" />
                  </Button>
                ) : null}
              </div>
            )}
            on="click"
            position="top center"
          />
        ));
      } else {
        phonemeScores = null;
      }
      return (
        <Segment
          raised
          className="report-phoneme-segment"
          key={`${wordScore.label}_${wordScore.score}`}
        >
          <h1 className="report-phoneme-segment-title">{wordScore.label}</h1>
          {phonemeScores}
        </Segment>
      );
    });
  }

  render() {
    const words = this.renderWords();
    const phonemes = this.renderPhonemes();

    return (
      <div className="Report">
        <Grid centered className="report-grid">
          <Grid.Row centered>
            <LaddaButton
              loading={this.state.playing}
              progress={this.state.progress}
              data-size={XL}
              data-style={EXPAND_RIGHT}
              data-spinner-size={30}
              data-spinner-color="#ddd"
              data-spinner-lines={12}
              onClick={this.playUtterance}
            >
              Play Back &nbsp;
                <Icon name="volume up" />
            </LaddaButton>
          </Grid.Row>
        </Grid>

        <Accordion styled fluid>
          <Accordion.Title
            active={this.state.activeIndex === 0}
            index={0}
            onClick={this.handleClickAccordion}
            className="report-accordion-title"
          >
            <Icon name="dropdown" />
            Word Scores
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 0}>{words}</Accordion.Content>

          <Accordion.Title
            active={this.state.activeIndex === 1}
            index={1}
            onClick={this.handleClickAccordion}
            className="report-accordion-title"
          >
            <Icon name="dropdown" />
            Phoneme Scores
          </Accordion.Title>
          <Accordion.Content active={this.state.activeIndex === 1}>{phonemes}</Accordion.Content>
        </Accordion>

        <Button
          style={{ marginTop: '16px', marginBottom: '16px' }}
          color="green"
          size="big"
          onClick={() => {
            this.props.reset();
          }}
        >
          Retry
        </Button>
      </div>
    );
  }
}

export default Report;
