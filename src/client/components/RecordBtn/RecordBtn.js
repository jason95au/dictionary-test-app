import React from 'react';
import PropTypes from 'prop-types';
import { Loader } from 'react-loaders';

import './RecordBtn.scss';

const display = (recorderState, toggleRecorder) => {
  let content;
  switch (recorderState) {
    case 'record':
      content = (
        <div className="record-btn">
          <div className="socket">
            <div className="button" role="button" tabIndex={0} onClick={toggleRecorder} />
          </div>
          <h1 className="record-btn-text">Record</h1>
        </div>
      );
      break;
    case 'recording':
      content = (
        <div className="record-btn">
          <div className="socket">
            <div className="button active" role="button" tabIndex={0} onClick={toggleRecorder} />
          </div>
          <h1 className="record-btn-text">Recording...</h1>
          <Loader type="ball-pulse" active />
        </div>
      );
      break;
    case 'analysing':
      content = (
        <div id="record-analysing-loader">
          <Loader type="line-scale-pulse-out" active />
          <h1>Analysing</h1>
        </div>
      );
      break;
    default:
      content = (
        <div className="record-btn">
          <div className="socket">
            <div className="button" role="button" tabIndex={0} onClick={toggleRecorder} />
          </div>
          <h1 className="record-btn-text">Record</h1>
        </div>
      );
  }
  return content;
};

const RecordBtn = ({ recorderState, toggleRecorder }) => (
  <div className="record-btn">
    {display(recorderState, toggleRecorder)}
  </div>
);

RecordBtn.propTypes = {
  recorderState: PropTypes.string.isRequired,
  toggleRecorder: PropTypes.func.isRequired,
};

export default RecordBtn;
