import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Mp3Recorder from '@articulate/rise-mp3-recorder';
import { Grid, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { getLCATEvaluation } from '../../redux/actions';
import RecordBtn from '../RecordBtn/RecordBtn';
import { capitalizeFirstLetter } from '../../helpers';
import { sentenceMap } from '../../helpers/constants';
import { GET_LCAT } from '../../redux/constants/action-types';
import Report from '../Report';

import './LCAT.scss';

class LCAT extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recorderState: 'record',
      displayLCAT: false
    };
    this.audioURL = null;
    this.toggleRecorder = this.toggleRecorder.bind(this);

    this.audioCtx = new window.AudioContext();
    const { sampleRate } = this.audioCtx;
    this.recorder = new Mp3Recorder({
      lameLibUrl: 'https://cdn.articulate.com/assets/rise/lame.min.js',
      sampleRate: sampleRate || 44100
    });
  }

  componentDidUpdate(prevProps) {
    // checks the called has been executed
    if (prevProps.isFetching[GET_LCAT] === true && this.props.isFetching[GET_LCAT] === false) {
      if (this.state.recorderState === 'analysing') {
        this.setState({ recorderState: 'record' });
      }
      if ('error' in this.props.LCATResult) {
        this.props._notificationSystem.addNotification({
          message: this.props.LCATResult.message,
          level: 'error',
          position: 'bl'
        });
      } else {
        // const blob = b64toBlob(this.mp3Base64, 'audio/mp3');
        // const blobUrl = URL.createObjectURL(blob);

        this.setState({
          displayLCAT: true
        });
      }
    }
  }

  toggleRecorder() {
    if (this.state.recorderState === 'record') {
      this.setState(
        {
          recorderState: 'recording'
        },
        () => {
          setTimeout(() => {
            this.recorder.start(
              () => { },
              err => {
                console.log(err);
              }
            );
          }, 300);
        }
      );
    } else if (this.state.recorderState === 'recording') {
      this.setState(
        {
          recorderState: 'analysing'
        },
        () => {
          setTimeout(() => {
            this.recorder.stop(() => {
              this.recorder.getMp3Blob(audio => {
                const mp3Base64 = audio.url.substr(22);
                this.audioURL = audio.url;
                const content = decodeURIComponent(sentenceMap[this.props.queryWord]);
                this.props.getLCATEvaluation(content, mp3Base64);
              });
            });
          }, 300);
        }
      );
    }
  }

  render() {
    const renderContent = (
      <Grid centered className="test-grid">
        <Grid.Row centered>
          <h1 className="test-title">Test</h1>
        </Grid.Row>
        <Grid.Row centered>
          <h1 className="test-text">Please Record your pronunciation of the content:</h1>
        </Grid.Row>
        <Grid.Row centered>
          <Segment raised className="test-segment">
            <h1 className="test-content">
              &quot;{capitalizeFirstLetter(sentenceMap[this.props.queryWord])}&quot;
            </h1>
          </Segment>
        </Grid.Row>
        <Grid.Row centered>
          <RecordBtn
            recorderState={this.state.recorderState}
            toggleRecorder={this.toggleRecorder}
          />
        </Grid.Row>
      </Grid>
    );

    const renderReport = (
      <Grid centered className="test-grid">
        <Grid.Row centered>
          <h1 className="test-title">Test</h1>
        </Grid.Row>
        <Grid.Row centered>
          <Report
            scores={this.props.LCATResult}
            audioURL={this.audioURL}
            reset={() => {
              this.setState({ displayLCAT: false });
            }}
          />
        </Grid.Row>
      </Grid>
    );

    if (this.state.displayLCAT) {
      return <div className="Test">{renderReport}</div>;
    }

    return <div className="Test">{renderContent}</div>;
  }
}

function mapStateToProps(state) {
  return {
    LCATResult: state.reducerLCAT,
    isFetching: state.reducerLoader
  };
}

LCAT.propTypes = {
  queryWord: PropTypes.string.isRequired
};

export default connect(
  mapStateToProps,
  { getLCATEvaluation }
)(LCAT);
