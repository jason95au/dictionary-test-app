/* eslint-disable */

import React, { Component } from 'react';
import './YouGlish.scss';

class YouGlish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    }
    this.widget = null;
    this.mountYouGlish = this.mountYouGlish.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.widget === null) {
      this.mountYouGlish();
    }
    if (this.props.selected === false) {
      this.widget.pause();
    }
  }

  mountYouGlish() {
    var views = 0, curTrack = 0, totalTracks = 0;

    // The API will call this method when switching to a new video. 
    function onVideoChange(event) {
      curTrack = event.trackNumber;
      views = 0;
    }

    this.widget = new YG.Widget("yg-widget-0", {
      width: 640,
      components: 584,
      events: {
        'onSearchDone': (event) => {
          if (event.totalResult === 0) alert("No result found");
          else totalTracks = event.totalResult;
        },
        'onVideoChange': onVideoChange,
        'onCaptionConsumed': () => {
          if (++views < 3) {
            this.widget.replay();
          } else {
            if (curTrack < totalTracks) {
              this.widget.next();
            }
          }
        },
        'onPlayerReady': () => {
          this.widget.pause();
        }
      }
    });

    // perform the initial search for queryWord
    this.widget.search(this.props.queryWord, "US");
  }

  render() {
    return (
      <div className={[this.props.selected ? 'c-youglish-container' : 'c-youglish-no-render']}>
        <div
          id="yg-widget-0"
        >
        </div>
      </div>
    );
  }
}

export default YouGlish;
