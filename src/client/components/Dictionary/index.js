import React, { Component } from 'react';
import { Dimmer, Loader, Icon, List, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Howl } from 'howler';

import './Dictionary.scss';
import { capitalizeFirstLetter } from '../../helpers';

import image from '../../../../public/images/oup-black.png';

class Dictionary extends Component {
  constructor(props) {
    super(props);
    this.renderAudio = this.renderAudio.bind(this);
    this.renderPhoneticSpelling = this.renderPhoneticSpelling.bind(this);
    this.renderlexicalEntries = this.renderlexicalEntries.bind(this);
    this.renderOrigin = this.renderOrigin.bind(this);
    this.howl = null;
  }

  renderPhoneticSpelling() {
    const { lexicalEntries } = this.props.wordInfo;
    if (
      lexicalEntries.length > 0 &&
      'pronunciations' in lexicalEntries[0] &&
      lexicalEntries[0].pronunciations.length > 0 &&
      'phoneticSpelling' in lexicalEntries[0].pronunciations[0]
    ) {
      return (
        <div className="c-dictionary-phonetic">
          [{lexicalEntries[0].pronunciations[0].phoneticSpelling} -{' '}
          {lexicalEntries[0].pronunciations[0].dialects[0]}]
        </div>
      );
    }
    return null;
  }

  renderOrigin() {
    const { lexicalEntries } = this.props.wordInfo;
    if (
      lexicalEntries.length > 0 &&
      'entries' in lexicalEntries[0] &&
      lexicalEntries[0].entries.length > 0 &&
      'etymologies' in lexicalEntries[0].entries[0]
    ) {
      return (
        <div>
          <div className="c-dictionary-lexicalCategory">Origin</div>
          <List as="ol">{lexicalEntries[0].entries[0].etymologies}</List>
        </div>
      );
    }
    return null;
  }

  renderlexicalEntries() {
    const { lexicalEntries } = this.props.wordInfo;
    return lexicalEntries.map(entry => {
      const { entries, lexicalCategory, derivatives } = entry;

      const wordDefinition = entries[0].senses.map(sense => {
        let renderExampleList = null;

        if ('examples' in sense) {
          const exampleArray = sense.examples.map(({ text }) => text);

          renderExampleList = (
            <List.Item
              as="ol"
              className="c-dictionary-lexicalExample"
              key={exampleArray.join(', ')}
            >
              {exampleArray.join(', ')}
            </List.Item>
          );
        }

        if ('definitions' in sense) {
          return (
            <List.Item as="li" key={sense.definitions[0]}>
              {sense.definitions[0]}
              {renderExampleList}
            </List.Item>
          );
        }
        return null;
      });

      return (
        <div>
          <div className="c-dictionary-lexicalCategory">{lexicalCategory}</div>
          <List as="ol">{wordDefinition}</List>
        </div>
      );
    });
  }

  renderAudio() {
    const { lexicalEntries } = this.props.wordInfo;
    if (
      lexicalEntries.length > 0 &&
      'pronunciations' in lexicalEntries[0] &&
      lexicalEntries[0].pronunciations.length > 0 &&
      'audioFile' in lexicalEntries[0].pronunciations[0]
    ) {
      this.howl = new Howl({
        src: [lexicalEntries[0].pronunciations[0].audioFile]
      });
      return (
        <Icon
          name="volume up"
          size="tiny"
          color="teal"
          className="c-dictionary-icon"
          onClick={() => {
            this.howl.play();
          }}
        />
      );
    }
    return null;
  }

  render() {
    if (this.props.wordInfo.wordID === null || this.props.isFetching) {
      return (
        <Dimmer active>
          <Loader>Loading</Loader>
        </Dimmer>
      );
    }

    const { wordID, word, lexicalEntries } = this.props.wordInfo;

    return (
      <div style={{ padding: '8px', position: 'relative', marginBottom: '12px' }}>
        <p className="c-poweredByOUP-watermark">
          <span>Powered by</span>
          <Image
            style={{ margin: 'auto', marginTop: '8px', borderRadius: '4px' }}
            src={image}
            size="small"
          />
        </p>

        <div className="c-dictionary-title">
          {capitalizeFirstLetter(word)}
          {this.renderAudio()}
        </div>
        {this.renderPhoneticSpelling()}
        {this.renderlexicalEntries()}
        {this.renderOrigin()}
      </div>
    );
  }
}

Dictionary.propTypes = {
  wordInfo: PropTypes.object.isRequired
};

export default Dictionary;
