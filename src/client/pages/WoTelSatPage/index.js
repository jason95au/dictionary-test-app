import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Container, Icon } from 'semantic-ui-react';
import NotificationSystem from 'react-notification-system';
import { NavLink } from 'react-router-dom';

import Dictionary from '../../components/Dictionary';
import YouGlish from '../../components/YouGlish';
import LCAT from '../../components/LCAT';
import { getWordInfo } from '../../redux/actions';
import { GET_WORD_DETAILS } from '../../redux/constants/action-types';
import './WoTelSatPage.scss';

class WoTelSatPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0
    };
    this._notificationSystem = null;
  }

  componentWillMount() {
    this.props.getWordInfo(this.props.match.params.phrase);
  }

  render() {
    return (
      <Container>
        <NotificationSystem ref={n => (this._notificationSystem = n)} />
        <Tabs
          selectedIndex={this.state.selectedIndex}
          onSelect={index => {
            this.setState({ selectedIndex: index });
          }}
        >
          <TabList className="tabList">
            <div className="c-search-button-woltelsat">
              <NavLink to="/">
                <span className="tabText">
                  <Icon name="search" />
                </span>
              </NavLink>
            </div>
            <Tab className={['tab', this.state.selectedIndex === 0 ? 'tab-selected' : '']}>
              <span className="tabText">Definition (定义)</span>
            </Tab>
            <Tab className={['tab', this.state.selectedIndex === 1 ? 'tab-selected' : '']}>
              <span className="tabText">Hear & See (听到并看到)</span>
            </Tab>
            <Tab className={['tab', this.state.selectedIndex === 2 ? 'tab-selected' : '']}>
              <span className="tabText">Speak (说话)</span>
            </Tab>
          </TabList>
          <div className="bar" />

          <TabPanel>
            <Dictionary
              wordInfo={this.props.oxfordWordInfo}
              isFetching={this.props.isFetching[GET_WORD_DETAILS]}
            />
          </TabPanel>
          <TabPanel forceRender>
            <YouGlish
              queryWord={this.props.match.params.phrase}
              selected={this.state.selectedIndex === 1}
            />
          </TabPanel>
          <TabPanel>
            <LCAT
              queryWord={this.props.match.params.phrase}
              _notificationSystem={this._notificationSystem}
            />
          </TabPanel>
        </Tabs>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    oxfordWordInfo: state.reducerWordInfo,
    isFetching: state.reducerLoader
  };
}

WoTelSatPage.propTypes = {
  match: PropTypes.object.isRequired,
  getWordInfo: PropTypes.func.isRequired,
  oxfordWordInfo: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps,
  { getWordInfo }
)(WoTelSatPage);
