import React, { Component } from 'react';
import { Search, Container, Label, Image } from 'semantic-ui-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import './SearchPage.scss';
import { possibleWords } from '../../redux/actions';
import { GET_SEARCH_WORD } from '../../redux/constants/action-types';

import image from '../../../../public/images/oup-white.png';

class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      loading: false
    };
    // auto search timeout
    this.timeout = 0;

    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.resultRenderer = this.resultRenderer.bind(this);
    this.handleResultSelect = this.handleResultSelect.bind(this);
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ value, loading: true }, () => {
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.props.possibleWords(this.state.value.toLocaleLowerCase());
        this.setState({ loading: false });
      }, 300);
    });
  };

  handleResultSelect(e, { result }) {
    const { description } = result;
    this.props.history.push({
      pathname: `/dictionary/${description}`
    });
  }

  resultRenderer({ title, description }) {
    return <Label as="span" key={description} content={title} />;
  }

  render() {
    const { value } = this.state;
    const showNoResults = Boolean(
      !this.props.isFetching[GET_SEARCH_WORD] &&
      this.props.oxfordSearchPhrase.results.length === 0 &&
      !this.state.loading
    );

    return (
      <Container>
        <div className="c-searchPage-color" />
        <div className="c-searchPage-background" />
        <div className="c-searchPage">
          <h1 className="c-searchPage-header">
            Language Confidence Speaking Dictionary (口语词典)
          </h1>
          <Search
            fluid
            loading={this.props.isFetching[GET_SEARCH_WORD]}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
            resultRenderer={this.resultRenderer}
            results={this.props.oxfordSearchPhrase.results}
            value={value}
            showNoResults={showNoResults}
            size="large"
          />
          <p className="c-searchPage-poweredBy">
            <span>Powered by</span>
            <Image
              style={{ margin: 'auto', marginTop: '8px', borderRadius: '4px' }}
              src={image}
              size="small"
            />
          </p>
        </div>
      </Container>
    );
  }
}

SearchPage.propTypes = {
  oxfordSearchPhrase: PropTypes.object.isRequired,
  isFetching: PropTypes.object.isRequired,
  possibleWords: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    oxfordSearchPhrase: state.reducerSearchWord,
    isFetching: state.reducerLoader
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    { possibleWords }
  )(SearchPage)
);
