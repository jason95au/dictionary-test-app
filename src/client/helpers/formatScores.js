const formatScores = (scores, content, thresholds) => {
  // Define Thresholds
  // TODO: get this from DB and make adjustable in Admin
  const phonemeThresGreen = thresholds.phonemeThresGreen;
  const phonemeThresOrange = thresholds.phonemeThresOrange;
  const wordThresOrange = thresholds.wordThresOrange;
  const wordThresGreen = thresholds.wordThresGreen;

  const missingWords = [];

  // filter out silences
  const filteredScores = scores.filter(scoreObj => scoreObj.label !== '<eps>');
  // add colorScore and handle missing words
  const finalScores = filteredScores.map((wordScore, index) => {
    let phonemeCount = 0;
    let orangeCount = 0;
    let redCount = 0;
    const rObj = wordScore;
    rObj.avgScore = 0;
    // if missing word set score to grey and remove phoneme scores
    if (wordScore.label === '<UNK>') {
      // determine word label from content string
      const contentArray = content.split(' ');
      const missingWord = contentArray[index];
      missingWords.push(missingWord);
      rObj.colorScore = 'grey';
      rObj.phonemes = null;
      rObj.label = missingWord.toUpperCase();
    } else {
      // phone score number sum
      let phoneNumberScoreSum = 0;
      // determine phoneme level scores
      rObj.phonemes = wordScore.phonemes.map((phonemeScore) => {

        // aggregate phone score
        rObj.avgScore += phonemeScore.score;

        const rPhonemeObj = phonemeScore;
        phonemeCount += 1;
        // add color scores
        let colorScore = 'red';
        if (phonemeScore.score > phonemeThresGreen) {
          colorScore = 'green';
        } else if (phonemeScore.score > phonemeThresOrange) {
          colorScore = 'orange';
          orangeCount += 1;
        } else {
          redCount += 1;
        }
        rPhonemeObj.colorScore = colorScore;

        // add number scores
        let numberScore = 0;
        switch (true) {
          case phonemeScore.score >= thresholds.phonemeThres8:
            numberScore = 8;
            break;
          case phonemeScore.score >= thresholds.phonemeThres7:
            numberScore = 7;
            break;
          case phonemeScore.score >= thresholds.phonemeThres6:
            numberScore = 6;
            break;
          case phonemeScore.score >= thresholds.phonemeThres5:
            numberScore = 5;
            break;
          case phonemeScore.score >= thresholds.phonemeThres4:
            numberScore = 4;
            break;
          case phonemeScore.score >= thresholds.phonemeThres3:
            numberScore = 3;
            break;
          case phonemeScore.score >= thresholds.phonemeThres2:
            numberScore = 2;
            break;
          case phonemeScore.score >= thresholds.phonemeThres1:
            numberScore = 1;
            break;
          default:
            numberScore = 0;
        }
        phoneNumberScoreSum += numberScore;
        rPhonemeObj.numberScore = numberScore;
        return rPhonemeObj;
      });

      // average phone score for word level
      rObj.avgScore = rObj.avgScore / phonemeCount;

      // average phone number score
      const phoneNumberScoreAvg = Math.round(phoneNumberScoreSum / phonemeCount);

      // determine word level color scores
      const correctPercentage = (phonemeCount - ((orangeCount * 0.75) + redCount)) / phonemeCount;
      let colorScore = 'red';
      if (correctPercentage >= wordThresGreen) {
        colorScore = 'green';
      } else if (correctPercentage >= wordThresOrange) {
        colorScore = 'orange';
      }

      // determine word level number scores
      rObj.numberScore = phoneNumberScoreAvg;
      rObj.colorScore = colorScore;
      rObj.score = correctPercentage;
    }
    return rObj;
  });
  return [finalScores, missingWords];
};

export default formatScores;
