import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import axios from 'axios';
import bodyParser from 'body-parser';

import { sentenceMap } from './constants';

// express & cors setup
const app = express();
app.use(cors());
app.use(express.static('dist'));

// parse application/json
const jsonParser = bodyParser.json();

// key extraction
dotenv.config();
const { OXFORDAPPID, OXFORDAPPKEY, LCATKEY } = process.env;
const BASE_URL = 'https://od-api.oxforddictionaries.com/api/v1';

app.get('/entries/en/:word_id', (req, res) => {
  axios({
    method: 'get',
    url: `/entries/en/${req.params.word_id}`,
    baseURL: BASE_URL,
    headers: {
      Accept: 'application/json',
      app_id: OXFORDAPPID,
      app_key: OXFORDAPPKEY
    }
  })
    .then(response => {
      res.status(200).json(response.data);
    })
    .catch(err => {
      res.status(500).json({ error: err.data });
    });
});

app.get('/search/en', (req, res) => {
  const query = req.query.q;
  const validWords = Object.keys(sentenceMap).map(word => {
    return {
      word,
      id: word,
      matchType: 'headword'
    };
  });
  const queryFilter = validWords.filter(({ word }) => word.includes(query));
  res.status(200).json({
    results: queryFilter
  });

  // FIXME: Commented out since we cannot use that set of info for LCAT just yet
  // axios({
  //   method: 'get',
  //   url: '/search/en',
  //   baseURL: BASE_URL,
  //   headers: {
  //     Accept: 'application/json',
  //     app_id: OXFORDAPPID,
  //     app_key: OXFORDAPPKEY
  //   },
  //   params: {
  //     limit: 5,
  //     prefix: false,
  //     q: req.query.q
  //   }
  // })
  //   .then(response => {
  //     res.status(200).json(response.data);
  //   })
  //   .catch(err => {
  //     res.status(500).json({ error: err.data });
  //   });
});

app.post('/test', jsonParser, (req, res) => {
  const content = req.body.content;

  axios({
    method: 'post',
    url: '/test/score',
    baseURL: 'http://api.languageconfidence.com',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': LCATKEY
    },
    data: {
      content: req.body.content,
      mp3Base64: req.body.mp3Base64
    }
  })
    .then(response => {
      res.status(200).json({ data: response.data, content });
    })
    .catch(() => {
      res.status(500).json({ error: 'Phoneme score prediction failed' });
    });
});

const server = app.listen(8080, () => console.log('Listening on port 8080!'));

module.exports = server;
