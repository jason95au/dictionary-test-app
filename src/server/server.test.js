import request from 'supertest';
import server from './index';

describe('express server', () => {
  afterEach(() => {
    server.close();
  });
  test('responds to /api/getArticles', async () => {
    const response = await request(server).get('/api/getArticles');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
  });
});
