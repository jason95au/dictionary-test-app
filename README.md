# LC React - Node.js Boilerplate

This is a sample project serving as boilerplate. It includes:

- A React Project structure with React Router and Redux
- An Express Node.js backend
- Support for Es6 JS with babel
- Scss modules and css autoprefixer
- Hot code reloading for both client and server
- Eslint setup with AirBnb styleguides
- Testing and code coverage with Jest and Enzyme

# Setup

1.  Clone or download repo contents
2.  Replace .git to new repository if needed
3.  Run npm install

#### VsCode Setup

VsCode should take the configuration from the workspace settings folder in .vscode/settings.json.
You just need to install some extensions to get the full functionality of some of the plugins:

- Prettier - Code formatter
- stylelint
- eslint

# Npm scripts

This project has some predefined scripts to run useful commands

```sh
npm run dev:client # starts the client side dev server with hot code reloading
npm run dev:server # starts the server side server with hot code reloading
npm run dev # runs both the client and server side together
npm start:server # starts the node.js server
npm run build:client # builds the client side code for production (in dist-client/)
npm run build:server # builds the server side for production (in dist-server/)
npm run test # runs all test files in the project
npm run test:all:watch # runs all test files in the project in watch mode
npm run test:client # run client side tests
npm run test:client:watch # run client side tests in watch mode
npm run test:server # run server side tests
npm run test:server:watch # run server side tests in watch mode
npm run test:coverage # run all tests and collect code coverage report
npm run test:coverage:client # run client tests and collect code coverage report
npm run test:coverage:server # run server tests and collect code coverage report
```

# Details

#### eslint

https://eslint.org/
Eslint is used to pick up on any coding or styling errors, with the editor plugin it will uderline(lint) and errors that need to be fixed.
The linting is based on the Airbnb rules https://github.com/airbnb/javascript

The goal is to have no active eslint errors, the deployment may be setup to fail on any eslint errors. So make sure to fix your errors by reading through the rules and chaging your code.

If you **Exceptionally** need to disable a rule there are multiple ways to do this:

- Disable the rule for the whole project in .eslintrc.json
- Disable the rule for a whole file with: `/* eslint-disable <rule> */` at the top of the file
- Disable the rule for a particular case with `// eslint-disable-line <rule>` or `// eslint-disable-next-line <rule>`

#### Babel

https://babeljs.io/
Babel compiles new javascript code (Es6) into older code to improve compatibility.
The configuration for babel is in .babelrc.

This project has two handy babel plugins:

- transform-class-properties: Solves the problem with .bind(this) in React component. Read more about it here: https://medium.com/@jacobworrel/babels-transform-class-properties-plugin-how-it-works-and-what-it-means-for-your-react-apps-6983539ffc22
- Allows the use of the spread operator (...) with objects: https://babeljs.io/docs/en/babel-plugin-transform-object-rest-spread/

#### Scss Modules

The webpack config enables the use of Scss modules. Uder the hood webpack processes scss files with the following pipeline:
Compile scss to css --> Apply autoprefixer --> Load css modules into 1 css bundle

This allows us to write scss modules in development but still have 1 css bundle in production for performance.

In React you can have your css modules next to your components:

```
components/
    - component.js
    - component.scss
```

You can import scss code within sccs with `@import 'file.scss'`
And you can apply your css styles within your react components like so:

```
...
import styles from 'component.scss'
...
render() {
    return (
      <h1 className={styles.header}>Some react component</h1>
...
```

#### webpack file-loader

https://github.com/webpack-contrib/file-loader
This plugin allows you to import files using the `import image from 'image.png'` syntax

#### Testing with JEST and Enzyme

https://jestjs.io/
https://github.com/airbnb/enzyme

This project uses the Jest testing framework. There are example tests in the repository. Jest will pick up and run any test file name with \*.test.js

You can use Enzyme to render Rect components into a virtual DOM.

Jest is setup to run code coverage reports as well, it is currently setup to fail if the code coverage is bellow 80%

The config for jest is is package.json

Some useful reads:

- https://alligator.io/react/testing-react-components/
- https://hackernoon.com/unit-testing-redux-connected-components-692fa3c4441c
- https://medium.com/@rickhanlonii/understanding-jest-mocks-f0046c68e53c
- https://medium.com/opendoor-labs/testing-react-components-with-jest-a7e8e4d312d8

# Google Analytics

This project uses the https://github.com/react-ga/react-ga package. To enable GA simply remove the comments and add your tracking ID in src/client/index.js

# Sentry

To add sentry support edit the comments in src/client/redux/store.js

# Contributing

If you see any errors or improvements that can be made, please submit a pull request :)
